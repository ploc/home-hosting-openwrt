This projet is a tutorial on how to configure openwrt to host your own servers at home.

Have a look at the commits:

- the commit message explains what is the commit for, and sometime how to do that using the web interface
- you can also have a look the the commit diff in order to do that with a terminal, or to check with a terminal that you have done the right operation by the web interface

Open an issue for any question.
